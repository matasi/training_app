require 'test_helper'

class PasswordResetMailerTest < ActionMailer::TestCase
  include Devise::Mailers::Helpers

  def setup
    ActionMailer::Base.deliveries = []
    Devise.mailer = 'Devise::Mailer'
    Devise.mailer_sender = 'password_resets@digitalacademy.ca'
    @account = accounts(:one)
    @account.send_reset_password_instructions
    @mail = ActionMailer::Base.deliveries.last
  end

  test "password_reset" do
    assert_equal "Reset password instructions", @mail.subject
    assert_equal [@account.email], @mail.to
    assert_equal ["password_resets@digitalacademy.ca"], @mail.from
    assert_match @account.first_name, @mail.body.encoded
  end

end
