require 'test_helper'

class AccountsControllerTest < ActionController::TestCase

  include Devise::TestHelpers

  setup do
    @account = Account.find_by email: 'test_account@test.com'
    sign_in @account
    @client = initialize_client
    request.env['devise.mapping'] = Devise.mappings[:account]
  end

  test "should get Edit Profile page" do
    get :edit_profile
    assert_response :success
  end

  test "should update profile" do
    patch :update_profile, account: {first_name: "Updated_first_name", last_name: "Updated_last_name", phone: "000" }
    assert_redirected_to root_path
    assert_equal assigns(:account)[:first_name], "Updated_first_name"
    sfdc_account = @client.find('Account', @account.id, 'Rails_ID__c')
    assert_equal sfdc_account.FirstName, "Updated_first_name"
  end

  test "should not update profile when first name is empty" do
    patch :update_profile, account: {first_name: "", last_name: "Updated_last_name", phone: "000" }
    assert_equal assigns(:account)[:first_name], "John"
  end

end
