json.array!(@incidents) do |incident|
  json.extract! incident, :id, :incident_number, :closed_date, :description, :is_closed, :status, :subject, :account_id
  json.url incident_url(incident, format: :json)
end
